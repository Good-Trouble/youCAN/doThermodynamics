using MarkdownLiteral: @mdx

macro css_str(stylesheet_url)
    the_css = open(download(stylesheet_url)) do io
        read(io, String)
    end
    return the_css
end

	
# ══ title-bloc ═══════════════════════════════════════════════════*═

macro title_str(title::String, bookID::String="") 
    cm_parser = CommonMark.Parser()
    enable!(cm_parser, AttributeRule)

    bookID == "" && return title |> cm_parser
    
    bib = Dict(
        "ms6" 	=> 	"""
                    Moran, MJ & HN Shapiro (2008) 
                    Fundamentals of engineering thermodynamics 6e, 
                    978-0471787358
                    https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/images/ms6cover.jpg
                    """,
        "ms8" 	=> 	"""
                    Moran, MJ Ed (2014) 
                    Fundamentals of engineering thermodynamics 8e, 
                    978-1118412930
                    https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/images/ms8cover.jpg
                    """,
        "ms9" 	=> 	"""
                    Moran, MJ, _et al_ (2018) 
                    Fundamentals of engineering thermodynamics 9e, 
                    978-1119391388
                    https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/images/ms9cover47x60.png
                    """,
        "cb7" 	=> 	"""
                    Çengel, YA & MA Boles (2011) 
                    Thermodynamics: An engineering approach 7e, 
                    978-0073529325
                    https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/images/cb7cover.jpg
                    """,
        "cb8" 	=> 	"""
                    Çengel, YA & MA Boles (2015) 
                    Thermodynamics: An engineering approach 8e, 
                    978-0073398174
                    https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/images/cb8cover.jpg
                    """,
        "cb9" 	=> 	"""
                    Çengel, YA, Boles, MA, & M Kanoglu (2019) 
                    Thermodynamics: An engineering approach 9e, 
                    978-1259822674
                    https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/images/cb9cover48x60.jpg
                    """,
    )

    haskey(bib, bookID) || return KeyError(bookID) 

    # the_bibliography, the_thumb = 
    #     @as bb bib[bookID] begin
    #         split(bb, '\n', keepempty=false)
    #         strip.(bb)
    #         (
    #             join(bb[1:3], " _", "_ "),
    #             # m("img", class="no-dark-invert", src=bb[4], title=join(bb[2:3]," "))
    #             """<img class="no-dark-invert" src=$(bb[4]) title="$(join(bb[2:3]," "))"> """
    #         )
    #     end


    the_bibliography, the_thumb = 
        bib[bookID] |>
            bb -> split(bb, '\n', keepempty=false) .|>                 # split into array
            strip |>               # Remove leading and trailing spaces from each element
            bb -> (                       # make tuple of bibliograpy and thumnal img tag
                join(bb[1:3], " _", "_ "), 
                # m("img", class="no-dark-invert", src=bb[4], title=join(bb[2:3]," "))
                """<img class="no-dark-invert" src="$(bb[4])" title="$(join(bb[2:3]," "))"> """ |> HTML
            )



    out = @mdx("""
        <div class="mcs title-bloc">
        <div id="title" class="mcs title">\n\n
        \n\n# $title\n\n{.source}\n$the_bibliography\n
        </div>
        $the_thumb
        </div>
        """)

    return out
end

  # ═════════════════════════════════════════════════════════════════*═

  spaces(n) = HTML("<br>"^n)
  space = spaces(1)

  # ═════════════════════════════════════════════════════════════════*═

"""
nthColor(colorscheme, position, color)
"""
function nthColor(darkmode, colorscheme, position, color)
    darkmode || return colorscheme

    return map(enumerate(colorscheme)) do (index, value)
        index == position ? color : value 
    end
end

