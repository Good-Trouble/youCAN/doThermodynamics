### A Pluto.jl notebook ###
# v0.19.3

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ a737bc16-cef7-11ec-14f3-05d33eddb235
begin
	using CommonMark, PlutoUI
	
	p = Parser()
		enable!(p, MathRule())

	make_me_gorgeous_baby = p
end;

# ╔═╡ c0902201-91c3-4e9f-a2f0-e4ee801b55b7
cm"""# one_conservation_eqn_demo.jl"""

# ╔═╡ 5e140e45-47a4-48af-b724-c73216598b57
"""
<style>
.julia-value input {
accent-color: var(--main-bg-color) ;
}
.cancel{fill: #fe4401}
</style>
""" |> HTML

# ╔═╡ ad627cf3-2d8c-469e-a739-b84e293640eb
begin
	_wout  = @bind wout  CheckBox()	
	_wbout = @bind wbout CheckBox()
	_ṁout  = @bind ṁout  CheckBox()
	_ėkout = @bind ėkout CheckBox(default=false)
	_ėpout = @bind ėpout CheckBox(default=false)
	_SSSF  = @bind SSSF  CheckBox()
end;

# ╔═╡ 5384e694-2abd-488a-95e2-6595d8abf477
cm"**SSSF:** $(_SSSF)"

# ╔═╡ c106ff47-65d8-44ad-85bb-a4f45a0e70e7
if ṁout
	cm"""
	**OUT:**{.outclass} 
	``\class{wbout}{\quad \sf \dot W_{b}}``     $(_wbout) 
	``\class{wout }{\quad \sf \dot W_{other}}`` $(_wout) 
	``\class{ṁout }{\quad \sf \dot m}`` $(_ṁout) ``\class{ṁout}{\Big(}``
	``\class{ėkout}{ \sf ke}``             $(_ėkout) 
	``\class{ėpout}{~~ \sf pe}`` $(_ėpout)  ``\class{ṁout}{\Big)}``  
	"""
else
	cm"""
	**OUT:**{.outclass} 
	``\class{wbout}{\quad \sf \dot W_{b}}``     $(_wbout) 
	``\class{wout}{\quad \sf \dot W_{other}}`` $(_wout) 
	``\class{ṁout}{\quad \sf \dot m}``         $(_ṁout) 
	"""
end

# ╔═╡ 0b76a16b-be5d-4fac-a3ef-9ec2bf800cd0
SSSFterm = !SSSF ? raw"\dv t E_{sys} " : raw"\CancelToo[cancel]{\sf 0~:~steady\ state}{\dv t} E_{sys} &=& 0"

# ╔═╡ db117e16-83f5-4d94-b773-7639f6f96e83
begin
	
	adv_o = [raw"\sum_{out}\dot m_{out}","","h"]

	ėkout && push!(adv_o, raw" + \textstyle\frac 12 \va{V}^2")
	ėpout && push!(adv_o, " + gz")

	length(adv_o) > 3 && begin
		adv_o[2] = raw"\left("
		push!(adv_o, raw"\right)")
	end

	push!(adv_o, "_{out}")
	adv_out = join(adv_o)
		
end

# ╔═╡ 8f2d77ba-9587-44db-9409-66aa7a69f382
begin
	out = []
	wbout && push!(out, raw"\dot W_{b,out}") 
	wout  && push!(out, raw"\dot W_{out}")
	ṁout  && push!(out, adv_out)
end

# ╔═╡ 87705f4f-e3ea-4329-9809-a1a59ea75f00
begin
	len_out = length(out)
	
	mainminus = string(
		"&",
		len_out > 0 ? "-" : "",
		"&")

	allout = len_out == 0 ? ""     : 
			 len_out == 1 ? out[1] : join(out, " - ")
	
	signed_out = string(
		mainminus,
		allout,
	);
end

# ╔═╡ 98ace8d2-4394-40d0-b7d1-05e23fc1058c
string(
raw"""
```math
\require{physics}
\newcommand{\CancelTo}[3][]{%
{\color{#1}\cancelto{\rlap{\textsf{$ \raise 0.5em {\,#2} $}}}{\style{fill:currentColor}{#3}}}}

\newcommand{\CancelToo}[3][]{%
{\class{#1}{\cancelto{\rlap{\textsf{$ \raise 0.5em {\,#2} $}}}{\style{fill:currentColor}{#3}}}}}


\begin{gather}
\dot E_{in} &-& \dot E_{out} &=& \dv t E_{sys}   \\[6pt]
\dot Q_{in}""",
signed_out,
"""
 &=& $(SSSFterm)
""",
raw"""
\end{gather}
```
"""
) |> make_me_gorgeous_baby

# ╔═╡ cdfa423b-e777-4bb6-b9df-cb1c340a1fd1


# ╔═╡ e6b15ce2-ec72-4a32-bfbc-c19a42c675e5
begin
	lit = "#00ff00"
	outclass = any([wbout, wout, ṁout, (ṁout & ėkout), (ṁout & ėpout)])
	"""
	<style>
	.wbout { fill: $(wbout ? lit : ""); }
	.wout  { font-weight:900;fill: $(wout  ? lit : "currentColor"); }
	.ṁout  { fill: $(ṁout  ? lit : "currentColor"); }
	.ėkout { fill: $(ėkout ? lit : "currentColor"); }
	.ėpout { fill: $(ėpout ? lit : "currentColor"); }
	.outclass { font-weight:900; color: $(outclass ? lit : "currentColor"); }
	</style>
	""" |> HTML
end

# ╔═╡ f83fd2dd-d44a-40b1-872a-e13a9f7192f5
outclass

# ╔═╡ 8c2a2658-9b8e-450e-b027-364d231a5d29
HTML("""
force dark mode bc i like it
<style>
    :root {
        --image-filters: invert(1) hue-rotate(180deg) contrast(0.8);
        --out-of-focus-opacity: 0.5;
        --main-bg-color: #1f1f1f;
        --rule-color: hsla(0, 0%, 100%, 0.15);
        --kbd-border-color: #222;
        --header-bg-color: #2a2928;
        --header-border-color: transparent;
        --ui-button-color: #fff;
        --cursor-color: #fff;
        --normal-cell: 100, 100, 100;
        --error-color: 255, 125, 125;
        --normal-cell-color: rgba(var(--normal-cell), 0.2);
        --dark-normal-cell-color: rgba(var(--normal-cell), 0.4);
        --selected-cell-color: rgb(40 147 189/65%);
        --code-differs-cell-color: #9b906c;
        --error-cell-color: rgba(var(--error-color), 0.6);
        --bright-error-cell-color: rgba(var(--error-color), 0.9);
        --light-error-cell-color: rgba(var(--error-color), 0);
        --export-bg-color: #262a36;
        --export-color: rgb(255 255 255/84%);
        --export-card-bg-color: #494949;
        --export-card-title-color: hsla(0, 0%, 100%, 0.85);
        --export-card-text-color: rgb(255 255 255/70%);
        --export-card-shadow-color: #0000001c;
        --pluto-schema-types-color: hsla(0, 0%, 100%, 0.6);
        --pluto-schema-types-border-color: hsla(0, 0%, 100%, 0.2);
        --pluto-dim-output-color: hsl(0, 0, 70%);
        --pluto-output-color: #c4c4c4;
        --pluto-output-h-color: #e6e6e6;
        --pluto-output-bg-color: var(--main-bg-color);
        --a-underline: #ffffff69;
        --blockquote-color: inherit;
        --blockquote-bg: #2e2e2e;
        --admonition-title-color: #000;
        --jl-message-color: #265a20;
        --jl-message-accent-color: #83bf8a;
        --jl-info-color: #2a4973;
        --jl-info-accent-color: #5c8ccd;
        --jl-warn-color: #605a22;
        --jl-warn-accent-color: #ddd464;
        --jl-danger-color: #642f27;
        --jl-danger-accent-color: #ff7562;
        --jl-debug-color: #522e5c;
        --jl-debug-accent-color: #c481df;
        --table-border-color: hsla(0, 0%, 100%, 0.2);
        --table-bg-hover-color: rgba(193, 192, 235, 0.15);
        --pluto-tree-color: rgb(209 207 207/61%);
        --disabled-cell-bg-color: hsla(0, 0%, 55%, 0.25);
        --selected-cell-bg-color: rgb(42 115 205/78%);
        --hover-scrollbar-color-1: rgba(0, 0, 0, 0.15);
        --hover-scrollbar-color-2: rgba(0, 0, 0, 0.05);
        --shoulder-hover-bg-color: hsla(0, 0%, 100%, 0.05);
        --pluto-logs-bg-color: #434351;
        --pluto-logs-progress-fill: #5f7f5b;
        --pluto-logs-progress-border: #9fb8d1;
        --nav-h1-text-color: #fff;
        --nav-filepicker-color: #b6b6b6;
        --nav-filepicker-border-color: #c7c7c7;
        --nav-process-status-bg-color: #525252;
        --nav-process-status-color: var(--pluto-output-h-color);
        --restart-recc-header-color: rgb(44 106 157/56%);
        --restart-req-header-color: rgb(145 66 60/56%);
        --dead-process-header-color: rgba(250, 75, 21, 0.473);
        --loading-header-color: rgba(51, 51, 51, 0.5);
        --disconnected-header-color: rgba(255, 169, 114, 0.56);
        --binder-loading-header-color: hsla(51, 64%, 90%, 0.5);
        --loading-grad-color-1: #a9d4f1;
        --loading-grad-color-2: #d0d4d7;
        --overlay-button-bg: #2c2c2c;
        --overlay-button-border: #c7a74670;
        --overlay-button-color: #fff;
        --input-context-menu-border-color: hsla(0, 0%, 100%, 0.1);
        --input-context-menu-bg-color: #27282f;
        --input-context-menu-soon-color: #b1b1b144;
        --input-context-menu-hover-bg-color: hsla(0, 0%, 100%, 0.1);
        --input-context-menu-li-color: #c7c7c7;
        --pkg-popup-bg: #3d2f44;
        --pkg-popup-border-color: #574f56;
        --pkg-popup-buttons-bg-color: var(--input-context-menu-bg-color);
        --black: #fff;
        --white: #000;
        --pkg-terminal-bg-color: #252627;
        --pkg-terminal-border-color: #c3c3c388;
        --pluto-runarea-bg-color: #2b2b2b;
        --pluto-runarea-span-color: #a89fa0;
        --dropruler-bg-color: hsla(0, 0%, 100%, 0.1);
        --jlerror-header-color: #d9baba;
        --jlerror-mark-bg-color: rgb(0 0 0/18%);
        --jlerror-a-bg-color: rgba(82, 58, 58, 0.5);
        --jlerror-a-border-left-color: #704141;
        --jlerror-mark-color: #b1a9a9;
        --helpbox-bg-color: #1e221f;
        --helpbox-box-shadow-color: #00000017;
        --helpbox-header-bg-color: #2c3e36;
        --helpbox-header-color: #fff8eb;
        --helpbox-notfound-header-color: #8b8b8b;
        --helpbox-text-color: #e6e6e6;
        --code-section-bg-color: #2c2c2c;
        --code-section-border-color: #555a64;
        --footer-color: #cacaca;
        --footer-bg-color: #26272c;
        --footer-atag-color: #72a1df;
        --footer-input-border-color: #6c6c6c;
        --footer-filepicker-button-color: #000;
        --footer-filepicker-focus-color: #9d9d9d;
        --footnote-border-color: rgba(114, 225, 231, 0.15);
        --undo-delete-box-shadow-color: hsla(240, 1%, 84%, 0.2);
        --cm-editor-tooltip-border-color: rgba(0, 0, 0, 0.2);
        --cm-editor-li-aria-selected-bg-color: #3271e7;
        --cm-editor-li-aria-selected-color: #fff;
        --cm-editor-li-notexported-color: hsla(0, 0%, 100%, 0.5);
        --code-background: #292d38;
        --cm-code-differs-gutters-color: rgb(235 213 28/11%);
        --cm-line-numbers-color: #8d86875e;
        --cm-selection-background: rgba(84, 139, 217, 0.48);
        --cm-selection-background-blurred: hsla(0, 0%, 59%, 0.48);
        --cm-editor-text-color: #ffe9fc;
        --cm-comment-color: #e96ba8;
        --cm-atom-color: #e46b58;
        --cm-number-color: #a57acd;
        --cm-property-color: #f99b15;
        --cm-keyword-color: #ff7a6f;
        --cm-string-color: #df7e4e;
        --cm-var-color: #afb7d3;
        --cm-var2-color: #06b6ef;
        --cm-macro-color: #82b38b;
        --cm-builtin-color: #5e7ad3;
        --cm-function-color: #f99b15;
        --cm-type-color: #94894c;
        --cm-bracket-color: #a2a273;
        --cm-tag-color: #ef6155;
        --cm-link-color: #815ba4;
        --cm-error-bg-color: #ef6155;
        --cm-error-color: #f7f7f7;
        --cm-matchingBracket-color: #fff;
        --cm-matchingBracket-bg-color: #c58c237a;
        --cm-placeholder-text-color: rgb(255 255 255/20%);
        --autocomplete-menu-bg-color: var(--input-context-menu-bg-color);
        --index-text-color: #c7c7c7;
        --index-clickable-text-color: #ebebeb;
        --docs-binding-bg: #323431;
        --cm-html-color: #00ab85;
        --cm-html-accent-color: #00e7b4;
        --cm-css-color: #ebd073;
        --cm-css-accent-color: #fffed2;
        --cm-css-why-doesnt-codemirror-highlight-all-the-text-aaa: #ffffea;
        --cm-md-color: #a2c9d5;
        --cm-md-accent-color: #00a9d1;
    }
</style>
""")

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
CommonMark = "a80b9123-70ca-4bc0-993e-6e3bcb318db6"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
CommonMark = "~0.8.6"
PlutoUI = "~0.7.38"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.CommonMark]]
deps = ["Crayons", "JSON", "URIs"]
git-tree-sha1 = "4cd7063c9bdebdbd55ede1af70f3c2f48fab4215"
uuid = "a80b9123-70ca-4bc0-993e-6e3bcb318db6"
version = "0.8.6"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.Crayons]]
git-tree-sha1 = "249fe38abf76d48563e2f4556bebd215aa317e15"
uuid = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
version = "4.1.1"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.URIs]]
git-tree-sha1 = "97bbe755a53fe859669cd907f2d96aee8d2c1355"
uuid = "5c2747f8-b7ea-4ff2-ba2e-563bfd36b1d4"
version = "1.3.0"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═c0902201-91c3-4e9f-a2f0-e4ee801b55b7
# ╠═f83fd2dd-d44a-40b1-872a-e13a9f7192f5
# ╠═5e140e45-47a4-48af-b724-c73216598b57
# ╟─0b76a16b-be5d-4fac-a3ef-9ec2bf800cd0
# ╟─5384e694-2abd-488a-95e2-6595d8abf477
# ╟─c106ff47-65d8-44ad-85bb-a4f45a0e70e7
# ╟─98ace8d2-4394-40d0-b7d1-05e23fc1058c
# ╟─a737bc16-cef7-11ec-14f3-05d33eddb235
# ╠═ad627cf3-2d8c-469e-a739-b84e293640eb
# ╠═8f2d77ba-9587-44db-9409-66aa7a69f382
# ╠═87705f4f-e3ea-4329-9809-a1a59ea75f00
# ╠═db117e16-83f5-4d94-b773-7639f6f96e83
# ╠═cdfa423b-e777-4bb6-b9df-cb1c340a1fd1
# ╠═e6b15ce2-ec72-4a32-bfbc-c19a42c675e5
# ╠═8c2a2658-9b8e-450e-b027-364d231a5d29
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
