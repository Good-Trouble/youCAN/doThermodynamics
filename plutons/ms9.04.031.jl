### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 8dae1488-fc90-46b9-9eec-75c29a0ddad2
begin
	using CommonMark
	HTML("<style>img {opacity:0.0;}</style>"); 
end

# ╔═╡ 76040982-617b-4692-baa2-b0b58830d6a6
using Unitful

# ╔═╡ e5cd0a9f-5b59-4eb1-8328-17775c8f7fd9
begin
	
  # load notebook infrastructure packages
	using HypertextLiteral
	using MarkdownLiteral: @mdx

	
  # create a special Commonmark parser for tricky MathJax interpolations
	mjxCMparser = Parser()
	enable!(mjxCMparser, MathRule())
	enable!(mjxCMparser, AttributeRule())
	enable!(mjxCMparser, AdmonitionRule())

	
  # load title and other text macros
	"https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/src/infrastructure.jl" |> download |> include
	
	
  # out message
	cm"""

	{.appendix-item .dull}
	_infrastructure_{.newthought} &emsp; packages
	"""
end

# ╔═╡ 93515bd6-fffd-4812-b931-81f4ddd39600
title"Problem 4.031"ms9

# ╔═╡ 2ca57c13-654d-48b4-9f87-c1672f21fd17
cm"""

{.center .w-50}
![turbine](https://imgur.com/IA54T7X.png)

{.fig_caption}
Steam turbine

"""

# ╔═╡ 8136dcfa-b9b2-4444-bb9e-b5b74645e4f7
cm"""
## Get Organized

Though we have a lot of terms to consider, the `Given` section was mighty generous. Let's dig in.
"""

# ╔═╡ c6183fab-017e-4033-b575-72bdf0d46bd3
space

# ╔═╡ b5532d62-442d-415f-a38d-e09db09ccff7
cm"""

**Mass**  
Trivial: One inlet, one outlet


**Energy**

```math
\newcommand{\CancelTo}[3][]{%
{\color{#1}\cancelto{\rlap{\textsf{~#2}}}{\style{fill:currentColor;}{#3}}}}

\require{physics}

\begin{gather}
\dot{E}_{in} &-& \dot{E}_{out} &=& \frac{\dd}{\dd t} E_{sys}  \\[6pt]

\dot{m} \left( h + ke + pe \right)_1 &-& 
\dot{m} \left( h + ke      \right)_2 - \dot{m} q_{out} - \dot{W}_{out} &=& 
\CancelTo[#db5728]{0~SSSF}{\dv t} E_{sys} \\[6pt] 

\end{gather}
```
"""

# ╔═╡ 14c615c5-243a-44fd-ae07-032f47c759f3
cm"""
Taking inventory, and marking unknown terms:

```math
\bbox[#00f900,6px]{\color{black} \dot{W}_{out} }  = 
\dot{m} \big( h + \bbox[#fddd23,4px]{\color{black} ke \vphantom{|}} 
                + \bbox[#fddd23,4px]{\color{black} pe \vphantom{|}} \big)_1 -
\dot{m} \big( h + \bbox[#fddd23,4px]{\color{black} ke \vphantom{|}} \big)_2 - \dot{m} q_{out} \\[18pt]{}
```

where

```math
ke = \frac{1}{2} \vec{V}^2  \qqtext{and} pe = gz \\[12pt]
```

Actually, we know everything we need to calculate ``\dot{W}_{out}``.
"""

# ╔═╡ 8df961d6-4152-4ace-ae4c-ca53d4993588
space

# ╔═╡ 5b066c04-4601-4f29-ab27-be295523a322
cm"""
## Attack 

This time, let's examine the relative contributions of each form of energy present.
"""

# ╔═╡ 866862ad-b70c-49ea-ba44-157f47f022b4
# Given

begin
	
	ṁ = 30u"kg/minute"
	h1 = 3100u"kJ/kg"
	V⃗1 = 30u"m/s"
	z1 = 3u"m"
	
	
	h2 = 2600u"kJ/kg"
	V⃗2 = 45u"m/s"
	
	q_out = 1.1u"kJ/kg"
	g = 9.81u"m/s^2"
	
end;

# ╔═╡ 4663132e-5bc0-444f-8afb-5222ec0712c6
cm"""
## Given 

Steam enters a turbine operating at steady state with a mass flow of $(ustrip(ṁ)) kg/min, a specific enthalpy of $(ustrip(h1)) kJ/kg, and a velocity of $(ustrip(V⃗1)) m/s. At the exit, the specific enthalpy is $(ustrip(h2)) kJ/kg and the velocity is $(ustrip(V⃗2)) m/s. The elevation of the inlet is $(z1) higher than at the exit. Heat transfer from the turbine to its surroundings occurs at a rate of $(ustrip(q_out)) kJ per kg of steam flowing. Let g = $(ustrip(g)) m/s². 
"""

# ╔═╡ 9da565a7-9c9c-41fd-a263-7be035bef03a
begin
	
	ke1 = V⃗1^2/2 |> u"kJ/kg"
	ke2 = V⃗2^2/2 |> u"kJ/kg"
	pe1 = g*z1   |> u"kJ/kg"
		
end;

# ╔═╡ 1d45806c-c431-49de-b47c-722e41695660
Ẇ_out = ṁ * (h1 + ke1 + pe1 - h2 - ke2 - q_out) 

# ╔═╡ 3a68f09d-ba35-498f-a643-b405e6b057db
Ẇ_out_rounded = round(typeof(1.0u"kW"), Ẇ_out, digits=4)

# ╔═╡ c050391b-4ea1-44ab-80f6-9a75d488f9b8
"""
## Find

Determine the power developed by the turbine, in kW.

{.message .found}
> **Found (spoiler)**
>
>  ``\\qquad \\dot W_{out} = \\pu{ $(Ẇ_out_rounded) }``
""" |> mjxCMparser

# ╔═╡ 6e6ebd05-d232-4632-9af8-d52343387ce7
space

# ╔═╡ 5aa13dd7-4e9a-4869-9aff-9c857f822699
begin
  # we strip the units for a nicer table
	(_ke1, _ke2, _pe1, _h1, _h2, _q_out)  = 
			(ke1, ke2, pe1, h1, h2, q_out) .|> ustrip
			
	cm"""
	### Relative contributions<br><br>
	

	{.table .caption}
	Relative energetic contributions, kJ/kg.
	
	{.this-table}
	|     |     h      |      ke      |    pe    | q<sub>out</sub> |
	|----:|:-----------|:-------------|:---------|:----------------|
	| in  |    $_h1    |    $_ke1     |  $_pe1   |        0        |
	| out |    $_h2    |    $_ke2     |    0     |     $_q_out     |
	|  Δ  | $(_h2-_h1) | $(_ke2-_ke1) | $(-_pe1) |     $_q_out     |
	
	<br>
	
	It usually takes high velocities and significant elevation changes for potential or kinetic energies to appear in magnitudes comparable to the energy carried by mass, particularly when steam is the working fluid. Keep in mind, _this logic is **only applicable to the differences**{.killerorange} of energetic property values (_eg_ ``ΔH``)_{.killerorange}, not the property values themselves (_eg_ ``H_1, H_2``).
	
	That said, don't forget that _we do generate PW of electricity via dams and wind turbines..._{.addblue} 
	
	<style>
	.this-table th {
		font-size: normal; 
		font-weight: 500;
	}
	.this-table td {
		font-family: Lato; 
	}
	.this-table tr:last-child {
	border-top: 1px solid currentColor;
	}
	.this-table, .this-table * {
		border-color: currentColor !important;
	}
	.this-table table {
		margin-top: 1em!important;
	}
	</style>
	"""
end

# ╔═╡ b9fcfd2a-6ac5-4d73-9551-53e15ce05fe5
space

# ╔═╡ fcac09a4-24ee-4d37-89f7-afb3eb3bb4b9
"""
!!! success "drop the mic"

    {.center}
    ``\\qquad \\dot W_{out} = \\pu{ $Ẇ_out_rounded }`` 

""" |> mjxCMparser

# ╔═╡ a6556945-2bb4-4ab8-8039-96aed5363b01
spaces(4)

# ╔═╡ 5f448fab-de12-409a-95c0-f8834217d3aa
cm"""
{.silly-heading-border}
## Appendix
"""

# ╔═╡ 37ec02d3-db16-4737-ace0-a7674f167ab6
# ═══ load css ════════════════════════════════════════════*═

@mdx("""

<style>
	(css"https://ualearn.blackboard.com/bbcswebdav/courses/202140-SS-ME-215-001-ME-215-002-ME-215-003/me215.202140/all_access/pluto-assets/styles/mcs_pluto_notebook.css")

	$(css"https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/styles/mcs.pluto.css")

	$(css"https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/styles/mcs.title-bloc.css")


	@media (prefers-color-scheme: dark) {
		img:not([class="no-dark-invert"]) {
			filter: invert(1) hue-rotate(180deg) !important;
		}

	}

    img {opacity: 1; transition: opacity ease 2s;}
</style>

{.appendix-item .dull}
_infrastructure_{.newthought} &emsp; styles
""")

# ╔═╡ fb2acc5a-f7df-4f67-9e85-ac77185dcf37
# TableOfContents()		# uncomment to show table of comments

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
CommonMark = "a80b9123-70ca-4bc0-993e-6e3bcb318db6"
HypertextLiteral = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
MarkdownLiteral = "736d6165-7244-6769-4267-6b50796e6954"
Unitful = "1986cc42-f94f-5a68-af5c-568840ba703d"

[compat]
CommonMark = "~0.8.6"
HypertextLiteral = "~0.9.3"
MarkdownLiteral = "~0.1.1"
Unitful = "~1.11.0"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

[[Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[CommonMark]]
deps = ["Crayons", "JSON", "URIs"]
git-tree-sha1 = "4cd7063c9bdebdbd55ede1af70f3c2f48fab4215"
uuid = "a80b9123-70ca-4bc0-993e-6e3bcb318db6"
version = "0.8.6"

[[CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[ConstructionBase]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "f74e9d5388b8620b4cee35d4c5a618dd4dc547f4"
uuid = "187b0558-2788-49d3-abe0-74a17ed4e7c9"
version = "1.3.0"

[[Crayons]]
git-tree-sha1 = "249fe38abf76d48563e2f4556bebd215aa317e15"
uuid = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
version = "4.1.1"

[[Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[MarkdownLiteral]]
deps = ["CommonMark", "HypertextLiteral"]
git-tree-sha1 = "0d3fa2dd374934b62ee16a4721fe68c418b92899"
uuid = "736d6165-7244-6769-4267-6b50796e6954"
version = "0.1.1"

[[Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[Parsers]]
deps = ["Dates"]
git-tree-sha1 = "85b5da0fa43588c75bb1ff986493443f821c70b7"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.2.3"

[[Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[URIs]]
git-tree-sha1 = "97bbe755a53fe859669cd907f2d96aee8d2c1355"
uuid = "5c2747f8-b7ea-4ff2-ba2e-563bfd36b1d4"
version = "1.3.0"

[[Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[Unitful]]
deps = ["ConstructionBase", "Dates", "LinearAlgebra", "Random"]
git-tree-sha1 = "b649200e887a487468b71821e2644382699f1b0f"
uuid = "1986cc42-f94f-5a68-af5c-568840ba703d"
version = "1.11.0"

[[libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
"""

# ╔═╡ Cell order:
# ╟─8dae1488-fc90-46b9-9eec-75c29a0ddad2
# ╟─93515bd6-fffd-4812-b931-81f4ddd39600
# ╟─4663132e-5bc0-444f-8afb-5222ec0712c6
# ╟─2ca57c13-654d-48b4-9f87-c1672f21fd17
# ╟─c050391b-4ea1-44ab-80f6-9a75d488f9b8
# ╟─8136dcfa-b9b2-4444-bb9e-b5b74645e4f7
# ╟─c6183fab-017e-4033-b575-72bdf0d46bd3
# ╟─b5532d62-442d-415f-a38d-e09db09ccff7
# ╟─14c615c5-243a-44fd-ae07-032f47c759f3
# ╟─8df961d6-4152-4ace-ae4c-ca53d4993588
# ╟─5b066c04-4601-4f29-ab27-be295523a322
# ╠═76040982-617b-4692-baa2-b0b58830d6a6
# ╠═866862ad-b70c-49ea-ba44-157f47f022b4
# ╠═9da565a7-9c9c-41fd-a263-7be035bef03a
# ╠═1d45806c-c431-49de-b47c-722e41695660
# ╠═3a68f09d-ba35-498f-a643-b405e6b057db
# ╟─6e6ebd05-d232-4632-9af8-d52343387ce7
# ╟─5aa13dd7-4e9a-4869-9aff-9c857f822699
# ╟─b9fcfd2a-6ac5-4d73-9551-53e15ce05fe5
# ╟─fcac09a4-24ee-4d37-89f7-afb3eb3bb4b9
# ╟─a6556945-2bb4-4ab8-8039-96aed5363b01
# ╟─5f448fab-de12-409a-95c0-f8834217d3aa
# ╟─e5cd0a9f-5b59-4eb1-8328-17775c8f7fd9
# ╟─37ec02d3-db16-4737-ace0-a7674f167ab6
# ╠═fb2acc5a-f7df-4f67-9e85-ac77185dcf37
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
