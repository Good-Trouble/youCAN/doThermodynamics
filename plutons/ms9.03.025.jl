### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ 7291ecff-dca9-4cf1-a2b4-97e97d41c98f
using CommonMark

# ╔═╡ 57b9638f-cd2e-4687-a85d-f7fb32dea1d0
using  Unitful

# ╔═╡ e748ed86-7bbc-4d30-b6de-283b680f60bb
begin
	
  # load notebook infrastructure packages
	using LaTeXStrings, 
		PlutoUI, 
	
		HypertextLiteral,
		Lazy,
		# MarkdownLiteral,
		Hyperscript,
		Random
		
	using MarkdownLiteral: @mdx

	
  # create a special Commonmark parser for tricky MathJax interpolations
	mjxCMparser = Parser()
	enable!(mjxCMparser, MathRule())
	enable!(mjxCMparser, AttributeRule())


  # load title and other text macros
	"https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/src/infrastructure.jl" |> download |> include
	
	
  # out message
	HTML("""
	<p class="appendix-item dull">
	  <span class="newthought">
		infrastructure 
	  </span>
	&emsp; packages
	</p>
	""")
	
end

# ╔═╡ 52866331-2bdb-4e0d-b8f6-5cfcdff56057
# saturation dome

begin
	
	using DataFrames, DataFramesMeta
	
	R134a_sat = DataFrame(
		Psat = [
			0.5164,0.6332,0.7704,0.9305,1.0199,1.1160,1.2192,1.3299,1.4483,1.5748,1.8540,2.1704,2.5274,2.9282,3.3765,3.8756,4.4294,5.0416,5.7160,6.4566,6.8530,7.2675,7.7006,8.1528,8.6247,9.1168,9.6298,10.164,10.720,11.299,12.526,13.851,15.278,16.813,21.162,26.324,32.435,39.742
		], #u"bar"
	
		vf = [
			0.7055e-03,0.7113e-03,0.7172e-03,0.7233e-03,0.7265e-03,0.7296e-03,0.7328e-03,0.7361e-03,0.7395e-03,0.7428e-03,0.7498e-03,0.7569e-03,0.7644e-03,0.7721e-03,0.7801e-03,0.7884e-03,0.7971e-03,0.8062e-03,0.8157e-03,0.8257e-03,0.8309e-03,0.8362e-03,0.8417e-03,0.8473e-03,0.8530e-03,0.8590e-03,0.8651e-03,0.8714e-03,0.8780e-03,0.8847e-03,0.8989e-03,0.9142e-03,0.9308e-03,0.9488e-03,1.0027e-03,1.0766e-03,1.1949e-03,1.5443e-03
		], #u"m^3/kg"
		
		vg = [
			0.3569,0.2947,0.2451,0.2052,0.1882,0.1728,0.1590,0.1464,0.1350,0.1247,0.1068,0.0919,0.0794,0.0689,0.0600,0.0525,0.0460,0.0405,0.0358,0.0317,0.0298,0.0281,0.0265,0.0250,0.0236,0.0223,0.0210,0.0199,0.0188,0.0177,0.0159,0.0142,0.0127,0.0114,0.0086,0.0064,0.0046,0.0027
		], #u"m^3/kg"
	)

	@mdx("""
	{.appendix-item}
	_data_{.newthought .appendix} &emsp; R-134a saturation dome
	""")
	
end

# ╔═╡ b1d420a2-92b9-4bf7-82b8-974e2bb0202f
title"Problem 03.025"ms9

# ╔═╡ d94ce667-2f71-4bcc-ad32-e7b03beaec6a
cm"""
## Given

As shown in **Fig. P3.25**{.addblue}, Refrigerant 134a is contained in a piston–cylinder assembly, initially as saturated vapor. The refrigerant is slowly heated until its temperature is 160 °C. During the process, the piston moves smoothly in the cylinder. 

<br>

{ .center .w-50 .ml-35 title="Textbook Figure P3.25" }
![Figure P3.25](https://imgur.com/11oMMd1.png) 


{ .fig_caption }
Figure P3.25 from the textbook.

"""

# ╔═╡ 420ac294-a186-4277-bd56-9c6334c8b9f8
cm"""

## Find

For the refrigerant, evaluate the work, in kJ/kg.

"""

# ╔═╡ ed396e65-e4eb-41ce-ba5f-7ed966ddacdd
spaces(n) = HTML("<br>"^n); space = spaces(1)

# ╔═╡ acc997cf-8244-4001-9caa-37f33ce947c3
cm"""

## Get Organized

_A little bit of thought_{.newthought} early on keeps us from running around aimlessly with our hair on fire.  Notice these things right away:

1. We're asked to find work. It's best to separate our forms of work into 

   ```math
   W = W_\text{boundary} + W_\text{every other kind of work}  
   ```

   {.greek}
   1. The moving piston suggests a volume change, so we definitely expect
      moving boundary work. Have no worries about being wrong; if it turns out 
      we have no volume change, the boundary work simply calculates to zero.
      For now, we just note our expression for boundary work and move on.

      ```math 
      W_{b,out} = \int P\, {\rm d}V
      ```

   1. We have no signs of shaft work, electricity, or any other forms of work, 
      so all of our work will be boundary work.

      ```math 
      W = W_{b,out}   
      ```

1. The units requested are kJ/kg, so we need to find 
   _mass specific_{.killerorange} work,``W/m``.

1. Let _...the piston moves smoothly in the cylinder_ scream to you:    
   **This is most probably an isobaric 
   process!!**{.killerorange style="font-weight:900;"}   

1. We are given the piston's weight and area. Those are the ingredients 
   of pressure. _We may not need it,_ but we have it. Stick it in your pocket.



{style="padding-block-start: 1em;"}
**SO WHAT DO WE NOW KNOW?**{.addblue}

We are calculating specific isobaric boundary work,

```math 
w = \int P\, {\rm d}v = P(v_2 - v_1),
```

so we need to find ``P, v_2,`` and ``v_1``, perform the calculation, and we're done.
"""

# ╔═╡ bf279795-d902-47c9-8352-e04c7c5c7aa6
space

# ╔═╡ cb438563-e0b4-480d-ab58-c8162717a886
cm"""

## Attack

Let's go find our three fugitive properties. The questions for you are the same every single time. _Was it given? Have I already calculated it? Do I have two independent properties so I can get it from the tables?_{.killerorange}


``\bf P``: **Calculate it.**

   Remember that piston weight and area? Don't forget the 
   contribution from the atmosphere... we work in absolute pressure 
   (except in very limited situations).

   ```math
   P_{sys} = P_{atm} + \frac{F_{piston}}{A_\textit{cross, piston}}
   ```

``\bf v_1``: **Thanks to ``P``, we have two independent properties.**  

   ```math
    v_1 = v(P, x_1)
   ```

   {style="text-align:center; padding-block-end:2em;"}
   ``P`` is calculated. ``x_1`` is given (saturated vapor).


``\bf v_2``: **Two independent properties.**  

   ```math
	\begin{equation}
    v_2 = v(P, T_2)
	\end{equation}
   ```

   {style="text-align:center;"}
   ``P`` is calculated. ``T_2 = 160\, \rm°C`` is given.

<br>
"""

# ╔═╡ 26ba1d81-a7a5-46e1-bbab-892a659eb18d
begin
	W_piston = 471.1u"N"
	D_piston = 0.02u"m"
	P_atm = 1u"bar"
	
	A_piston = π/4 * D_piston^2
	
end;

# ╔═╡ 491f71f3-1dc9-48d6-852b-a09b1663e658
P = P_atm + W_piston/A_piston |> u"bar"

# ╔═╡ 65dded8b-1197-432e-93ea-d02f9d6c775a
begin
	v1 = 0.0121u"m^3/kg"         # Table A-11
	v2 = 0.02017u"m^3/kg"        # Table A-12
end;

# ╔═╡ fc33d878-71a7-443f-9157-f905ccded4c7
"""
I'm pretty sure P = $(P) can be safely rounded to 16 bar for our dives into the tables: 

{.message .danger} 
> actually, **do not round** if your property pair is ``P, T`` and you're testing for saturation conditions. Those ``(T_{sat},P_{sat})`` combinations must be **exact**.

 - From `Table A-11`{.mcs .highlight .property-table}, 
   ``v_1 = v_{\\rm g@16bar} = \\pu{ $(v1 |> u"m^3/kg" |> ustrip) m³/kg}``

 - From `Table A-12`{.mcs .highlight .property-table}, 
   ``v_2 = v_{\\rm g@16bar} = \\pu{ $(v2 |> u"m^3/kg" |> ustrip) m³/kg}``

""" |> mjxCMparser

# ╔═╡ 74975d87-9999-4ef9-bf62-a0294d60c511
w = P * (v2 - v1) |> u"kJ/kg"

# ╔═╡ 58bb2681-9699-477a-8fb2-fdf4208d48f7
w_rounded = round(typeof(w), w, digits=4)

# ╔═╡ 5bffc527-b8f7-4fbf-bbff-b5ae8a5a4178
space

# ╔═╡ 5104846f-0dc1-4711-b948-77c69a9db2df
cm"""

_Get in the habit of plotting your processes_{.newthought}, even if it's only a sketch. You gain a deeper understanding of the processes involved, and plots give insights that lead to faster, more robust solutions... **especially**{.killerorange} when you sketch your plots while you're getting organized. 

For example, in the current scenario, we knew from the problem statement that our process began on the saturated vapor line, and we knew we had an isobaric process. 

Mouse over the plot to find and play with its features.
"""

# ╔═╡ f9a5c3f0-5e05-45da-acfc-e03e81a7b36d
cm"""
<br><br>

{.finis}
### _finis_

{.message .success}
> w = $(w_rounded |> ustrip) kJ/kg
> {.addblue}
> ``\quad`` _Go read [**Find**](#find){.mcs .faux-heading .font-style-normal} again. Ensure you're reporting what was actually requested._
"""

# ╔═╡ 9cfeb687-dca0-41b7-bcf0-b08d29081c23
# HTML("""
# <style>
# .plotly svg.main-svg .annotation-text {
# 	fill: var(--audiblegreen)!important;
# }
# .plotly svg.main-svg .trace:nth-child(3) path {
# 	stroke: var(--audiblegreen)!important;
# 	fill: var(--audiblegreen)!important;
# }
# </style>
# """)

# ╔═╡ a225f838-bd6e-425d-b531-d7adf7f3768d
cm"""
&nbsp;

{.count-appendix .silly-heading-border}
## Appendix

One of the major benefits of reactive notebooks like Pluto is that allow large, unwieldy chunks of code to be placed outside the reading flow without removing them from their proper place in the computation flow. Their results can be used in calculations or spliced into the narrative (eg, the _P-v_ plot). 

{.center .appendix}
_Those code chunks live here._

<br>
"""

# ╔═╡ 54b043c6-7404-448c-8547-afa4288adab2
# ═══ load css ════════════════════════════════════════════*═

@mdx("""

<style>

	$(css"https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/styles/mcs.pluto.css")

	$(css"https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/styles/mcs.title-bloc.css")


	@media (prefers-color-scheme: dark) {
		img:not([class="no-dark-invert"]) {
			filter: invert(1) hue-rotate(180deg) !important;
		}

	}

</style>

{.appendix-item .dull}
_infrastructure_{.newthought} &emsp; styles
""")

# ╔═╡ 6a10a864-8441-4861-ac31-ced59bf854ac
begin

	R134a = @subset(R134a_sat, :vg .< 0.035)
	
	vfColorLD = ("#037db6","#35abda")
	vf = (
		x = R134a.vf,
		y = R134a.Psat,
		name = "vf",
		type = "scatter",
		mode = "lines",
	)

	vgColorLD = ("#037db6","#35abda")
	vg = (
		x = R134a.vg,
		y = R134a.Psat,
		name = "vg",
		type = "scatter",
		mode = "lines",
	)

	processColorLD = ("#00b800","#00f900")
	processFillColorLD = ["#00b80044","#7ffc7f44"];
	process = (
		x = [v1, v2] .|> ustrip,
		y = [ P,  P] .|> ustrip,
		name = "process",
		type = "scatter",
		mode = "lines+markers+text",
		marker = (size = 9,),
		text = ("1","2"),
		textfont = (
			family = "Lato",
			size = 14,
		),
		textposition = ("top left", "top right"),
		fill = "tozeroy",
	)

	processText = (
		x = [(v1+v2)/2] .|> ustrip,
		y = [P/2] .|> ustrip,
		name = "process",
		type = "scatter",
		mode = "text",
		text = ("work"),
		textfont = (
			family = "Lato",
			size = 14,
		),
	)

	data = [vf, vg, process, processText]	

	
	p = @mdx("""<div id="myDiv"></div>""")

	@mdx("""
	{.appendix-item .dull}
	_infrastructure_{.newthought} &emsp; julia: Plotly traces
	""")

end

# ╔═╡ ba11c481-bd6c-449b-aaf3-b0609582e371
cm"""

{.center title="Plot of the process and associated work"}
$(p) 

{ .fig_caption }
The process and its associated work.

"""

# ╔═╡ 0dafe467-fb06-44e6-91fd-c643df313a9e
# Set up the plot in Julia. 

# Write the plot directly into Javascript.

begin
	
	mainColorLD   = ("#404040", "#a4a4a4")
	mainBGcolorLD = ("#ffffff", "#1f1f1f")
	# mainBGcolorLD = ("#ff0000", "#ffff00")
	
	xaxis = (
		title = "specific volume, m³/kg",
	    tickfont = (
			family = "Lato",
			size   = 12,
		),
		ticks = "outside",
		tick0 = 0,
		dtick = 0.005,
		range = (-0.001, 0.0225),
		# rangeslider = (visible = true,),
		showline = false,
		zeroline = false,
		domain = (0, 1),
	)

	yaxis = (
		title = "pressure, bar",
	    tickfont = (
			family = "Lato",
			size   = 12,
		),
		ticks = "outside",
		tick0 = 0,
		dtick = 10,
		range = (-3, 43),
		showline = false,
		zeroline = false,
		domain = (0, 1),
	)

	
	layout = (
		font  = (
			family = "Alegreya Sans",
			size = 12,
		),
		margin = (t = 0,),
		showlegend = false,
		legend = (orientation = "h",),
		width = 675,
		xaxis,
		yaxis,
	)

	config = (
		toImageButtonOptions = (
			format = "svg", # // one of png, svg, jpeg, webp
			filename = "custom_image",
			height = 500,
			width = 700,
			scale = 1,		#// Multiply title/legend/axis/canvas sizes by this factor
		),	
	)



# write it all to js with the light/dark coloring logic
	@mdx("""
	<script src="https://cdn.plot.ly/plotly-2.11.1.min.js"></script>
	
	<script>
		Plotly.newPlot('myDiv', $data, $layout, $config, {responsive: true});
	</script>
	
	
	<script>
		const modeLD = ['Light mode','Dark mode'];
		const mainColorLD   = $mainColorLD;
		const mainBGcolorLD = $mainBGcolorLD;
		const vfColorLD = $vfColorLD;
		const vgColorLD = $vgColorLD;
		const processColorLD = $processColorLD;
		const processFillColorLD = $processFillColorLD;
		
		
		// light/dark styling logic
		
		const colorSchemeQueryList = window.matchMedia('(prefers-color-scheme: dark)');
		  
		const setColorScheme = e => {
	
			//	if (e.matches) {
			//	 // Dark
			//		console.log('Dark mode')
			//	} else {
			//	 // Light
			//		console.log('Light mode')
			//	}
		
		
			var dark1 = e.matches ? 1 : 0;
	
			console.log(modeLD[dark1])
		
			var updateTrace0 = {
				'marker.color' : vfColorLD[dark1],
			};
		
			var updateTrace1 = {
				'marker.color' : vgColorLD[dark1],
			};	
	
			var updateTrace2 = {
				'marker.color'   : processColorLD[dark1],
				'textfont.color' : processColorLD[dark1],
				'fillcolor'      : processFillColorLD[dark1],
			};
		
			var updateTrace3 = {
				'textfont.color' : processColorLD[dark1],
			};
		
			var updateLayout = {
				'title.font.color' : mainColorLD[dark1],
				'plot_bgcolor'     : mainBGcolorLD[dark1],
				'paper_bgcolor'    : mainBGcolorLD[dark1],
				'xaxis.color'      : mainColorLD[dark1],
				'yaxis.color'      : mainColorLD[dark1],
			};
	
	
		// save one function call by combining the last trace 
		// restyle with the relayout via Plotly.update
	
			Plotly.restyle('myDiv', updateTrace0, 0);
			Plotly.restyle('myDiv', updateTrace1, 1);
			Plotly.restyle('myDiv', updateTrace2, 2);
			Plotly.restyle('myDiv', updateTrace3, 3);
			Plotly.relayout('myDiv', updateLayout);
		
		//	console.log("mainBGcolor  :", mainBGcolorLD[dark1] );
		//	console.log("mainColor    :", mainColorLD[dark1]   );
		//	console.log("updateLayout :", updateLayout);
	
		};
		  
		setColorScheme(colorSchemeQueryList);
		colorSchemeQueryList.addEventListener('change', setColorScheme);
	</script>

	{.appendix-item .dull}
	_infrastructure_{.newthought} &emsp; javascript: Plotly layout and light/dark mode styling
	""");
end

# ╔═╡ 6c9eaa0e-d1b5-4442-993e-d1b6ae15a27d
# begin

# 	macro css_str(stylesheet_url)
# 		the_css = open(download(stylesheet_url)) do io
# 			read(io, String)
# 		end
# 		return the_css
# 	end

	
#   # ══ title-bloc ═══════════════════════════════════════════════════*═

# 	macro title_str(title::String, bookID::String="") 
# 		cm_parser = CommonMark.Parser()
# 		enable!(cm_parser, AttributeRule)
	
# 		bookID == "" && return title |> cm_parser
		
# 		bib = Dict(
# 			"ms6" 	=> 	"""
# 						Moran, MJ & HN Shapiro (2008) 
# 						Fundamentals of engineering thermodynamics 6e, 
# 						978-0471787358
# 						https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/images/ms6cover.jpg
# 						""",
# 			"ms8" 	=> 	"""
# 						Moran, MJ Ed (2014) 
# 						Fundamentals of engineering thermodynamics 8e, 
# 						978-1118412930
# 						https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/images/ms8cover.jpg
# 						""",
# 			"ms9" 	=> 	"""
# 						Moran, MJ, _et al_ (2018) 
# 						Fundamentals of engineering thermodynamics 9e, 
# 						978-1119391388
# 						https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/images/ms9cover47x60.png
# 						""",
# 			"cb7" 	=> 	"""
# 						Çengel, YA & MA Boles (2011) 
# 						Thermodynamics: An engineering approach 7e, 
# 						978-0073529325
# 						https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/images/cb7cover.jpg
# 						""",
# 			"cb8" 	=> 	"""
# 						Çengel, YA & MA Boles (2015) 
# 						Thermodynamics: An engineering approach 8e, 
# 						978-0073398174
# 						https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/images/cb8cover.jpg
# 						""",
# 			"cb9" 	=> 	"""
# 						Çengel, YA, Boles, MA, & M Kanoglu (2019) 
# 						Thermodynamics: An engineering approach 9e, 
# 						978-1259822674
# 						https://gitlab.com/Good-Trouble/youCAN/doThermodynamics/-/raw/main/images/cb9cover48x60.jpg
# 						""",
# 		)
	
# 		haskey(bib, bookID) || return KeyError(bookID) 
	
# 		the_bibliography, the_thumb = 
# 			@as bb bib[bookID] begin
# 				split(bb, '\n', keepempty=false)
# 				strip.(bb)
# 				(
# 					join(bb[1:3], " _", "_ "),
# 					m("img", class="no-dark-invert", src=bb[4], title=join(bb[2:3]," "))
	
# 				)
# 			end
		
# 		out = @mdx("""
# 			<div class="mcs title-bloc">
# 			<div id="title" class="mcs title">\n\n
# 			\n\n# $title\n\n{.source}\n$the_bibliography\n
# 			</div>
# 			$the_thumb
# 			</div>
# 			""")
	
# 		return out
# 	end

#   # ═════════════════════════════════════════════════════════════════*═

	@mdx("""
	{.appendix-item .dull}
	_infrastructure_{.newthought} &emsp; macros
	""")
	
# end

# ╔═╡ 8e4c40f3-26f0-4fb4-b7ae-5c47e7de1cd3
# TableOfContents()		# uncomment to show table of comments

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
CommonMark = "a80b9123-70ca-4bc0-993e-6e3bcb318db6"
DataFrames = "a93c6f00-e57d-5684-b7b6-d8193f3e46c0"
DataFramesMeta = "1313f7d8-7da2-5740-9ea0-a2ca25f37964"
Hyperscript = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
HypertextLiteral = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
LaTeXStrings = "b964fa9f-0449-5b57-a5c2-d3ea65f4040f"
Lazy = "50d2b5c4-7a5e-59d5-8109-a42b560f39c0"
MarkdownLiteral = "736d6165-7244-6769-4267-6b50796e6954"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
Random = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"
Unitful = "1986cc42-f94f-5a68-af5c-568840ba703d"

[compat]
CommonMark = "~0.8.6"
DataFrames = "~1.3.2"
DataFramesMeta = "~0.10.0"
Hyperscript = "~0.0.4"
HypertextLiteral = "~0.9.3"
LaTeXStrings = "~1.3.0"
Lazy = "~0.15.1"
MarkdownLiteral = "~0.1.1"
PlutoUI = "~0.7.23"
Unitful = "~1.11.0"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.Chain]]
git-tree-sha1 = "339237319ef4712e6e5df7758d0bccddf5c237d9"
uuid = "8be319e6-bccf-4806-a6f7-6fae938471bc"
version = "0.4.10"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.CommonMark]]
deps = ["Crayons", "JSON", "URIs"]
git-tree-sha1 = "4cd7063c9bdebdbd55ede1af70f3c2f48fab4215"
uuid = "a80b9123-70ca-4bc0-993e-6e3bcb318db6"
version = "0.8.6"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.ConstructionBase]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "f74e9d5388b8620b4cee35d4c5a618dd4dc547f4"
uuid = "187b0558-2788-49d3-abe0-74a17ed4e7c9"
version = "1.3.0"

[[deps.Crayons]]
git-tree-sha1 = "249fe38abf76d48563e2f4556bebd215aa317e15"
uuid = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
version = "4.1.1"

[[deps.DataAPI]]
git-tree-sha1 = "cc70b17275652eb47bc9e5f81635981f13cea5c8"
uuid = "9a962f9c-6df0-11e9-0e5d-c546b8b5ee8a"
version = "1.9.0"

[[deps.DataFrames]]
deps = ["Compat", "DataAPI", "Future", "InvertedIndices", "IteratorInterfaceExtensions", "LinearAlgebra", "Markdown", "Missings", "PooledArrays", "PrettyTables", "Printf", "REPL", "Reexport", "SortingAlgorithms", "Statistics", "TableTraits", "Tables", "Unicode"]
git-tree-sha1 = "ae02104e835f219b8930c7664b8012c93475c340"
uuid = "a93c6f00-e57d-5684-b7b6-d8193f3e46c0"
version = "1.3.2"

[[deps.DataFramesMeta]]
deps = ["Chain", "DataFrames", "MacroTools", "OrderedCollections", "Reexport"]
git-tree-sha1 = "ab4768d2cc6ab000cd0cec78e8e1ea6b03c7c3e2"
uuid = "1313f7d8-7da2-5740-9ea0-a2ca25f37964"
version = "0.10.0"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.DataValueInterfaces]]
git-tree-sha1 = "bfc1187b79289637fa0ef6d4436ebdfe6905cbd6"
uuid = "e2d170a0-9d28-54be-80f0-106bbe20a464"
version = "1.0.0"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Formatting]]
deps = ["Printf"]
git-tree-sha1 = "8339d61043228fdd3eb658d86c926cb282ae72a8"
uuid = "59287772-0a20-5a39-b81b-1366585eb4c0"
version = "0.4.2"

[[deps.Future]]
deps = ["Random"]
uuid = "9fa8497b-333b-5362-9e8d-4d0656e87820"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.InvertedIndices]]
git-tree-sha1 = "bee5f1ef5bf65df56bdd2e40447590b272a5471f"
uuid = "41ab1584-1d38-5bbf-9106-f11c6c58b48f"
version = "1.1.0"

[[deps.IteratorInterfaceExtensions]]
git-tree-sha1 = "a3f24677c21f5bbe9d2a714f95dcd58337fb2856"
uuid = "82899510-4779-5014-852e-03e436cf321d"
version = "1.0.0"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LaTeXStrings]]
git-tree-sha1 = "f2355693d6778a178ade15952b7ac47a4ff97996"
uuid = "b964fa9f-0449-5b57-a5c2-d3ea65f4040f"
version = "1.3.0"

[[deps.Lazy]]
deps = ["MacroTools"]
git-tree-sha1 = "1370f8202dac30758f3c345f9909b97f53d87d3f"
uuid = "50d2b5c4-7a5e-59d5-8109-a42b560f39c0"
version = "0.15.1"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.MacroTools]]
deps = ["Markdown", "Random"]
git-tree-sha1 = "3d3e902b31198a27340d0bf00d6ac452866021cf"
uuid = "1914dd2f-81c6-5fcd-8719-6d5c9610ff09"
version = "0.5.9"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MarkdownLiteral]]
deps = ["CommonMark", "HypertextLiteral"]
git-tree-sha1 = "0d3fa2dd374934b62ee16a4721fe68c418b92899"
uuid = "736d6165-7244-6769-4267-6b50796e6954"
version = "0.1.1"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Missings]]
deps = ["DataAPI"]
git-tree-sha1 = "bf210ce90b6c9eed32d25dbcae1ebc565df2687f"
uuid = "e1d29d7a-bbdc-5cf2-9ac0-f12de2c33e28"
version = "1.0.2"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "621f4f3b4977325b9128d5fae7a8b4829a0c2222"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.2.4"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.PooledArrays]]
deps = ["DataAPI", "Future"]
git-tree-sha1 = "28ef6c7ce353f0b35d0df0d5930e0d072c1f5b9b"
uuid = "2dfb63ee-cc39-5dd5-95bd-886bf059d720"
version = "1.4.1"

[[deps.PrettyTables]]
deps = ["Crayons", "Formatting", "Markdown", "Reexport", "Tables"]
git-tree-sha1 = "dfb54c4e414caa595a1f2ed759b160f5a3ddcba5"
uuid = "08abe8d2-0d0c-5749-adfa-8a2ac140af0d"
version = "1.3.1"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SortingAlgorithms]]
deps = ["DataStructures"]
git-tree-sha1 = "b3363d7460f7d098ca0912c69b082f75625d7508"
uuid = "a2af1166-a08f-5f64-846c-94a0d3cef48c"
version = "1.0.1"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.TableTraits]]
deps = ["IteratorInterfaceExtensions"]
git-tree-sha1 = "c06b2f539df1c6efa794486abfb6ed2022561a39"
uuid = "3783bdb8-4a98-5b6b-af9a-565f29a5fe9c"
version = "1.0.1"

[[deps.Tables]]
deps = ["DataAPI", "DataValueInterfaces", "IteratorInterfaceExtensions", "LinearAlgebra", "OrderedCollections", "TableTraits", "Test"]
git-tree-sha1 = "5ce79ce186cc678bbb5c5681ca3379d1ddae11a1"
uuid = "bd369af6-aec1-5ad0-b16a-f7cc5008161c"
version = "1.7.0"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.URIs]]
git-tree-sha1 = "97bbe755a53fe859669cd907f2d96aee8d2c1355"
uuid = "5c2747f8-b7ea-4ff2-ba2e-563bfd36b1d4"
version = "1.3.0"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Unitful]]
deps = ["ConstructionBase", "Dates", "LinearAlgebra", "Random"]
git-tree-sha1 = "b649200e887a487468b71821e2644382699f1b0f"
uuid = "1986cc42-f94f-5a68-af5c-568840ba703d"
version = "1.11.0"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╟─b1d420a2-92b9-4bf7-82b8-974e2bb0202f
# ╟─7291ecff-dca9-4cf1-a2b4-97e97d41c98f
# ╟─d94ce667-2f71-4bcc-ad32-e7b03beaec6a
# ╟─420ac294-a186-4277-bd56-9c6334c8b9f8
# ╟─ed396e65-e4eb-41ce-ba5f-7ed966ddacdd
# ╟─acc997cf-8244-4001-9caa-37f33ce947c3
# ╟─bf279795-d902-47c9-8352-e04c7c5c7aa6
# ╟─cb438563-e0b4-480d-ab58-c8162717a886
# ╠═57b9638f-cd2e-4687-a85d-f7fb32dea1d0
# ╠═26ba1d81-a7a5-46e1-bbab-892a659eb18d
# ╠═491f71f3-1dc9-48d6-852b-a09b1663e658
# ╟─fc33d878-71a7-443f-9157-f905ccded4c7
# ╠═65dded8b-1197-432e-93ea-d02f9d6c775a
# ╠═74975d87-9999-4ef9-bf62-a0294d60c511
# ╠═58bb2681-9699-477a-8fb2-fdf4208d48f7
# ╟─5bffc527-b8f7-4fbf-bbff-b5ae8a5a4178
# ╟─5104846f-0dc1-4711-b948-77c69a9db2df
# ╟─ba11c481-bd6c-449b-aaf3-b0609582e371
# ╟─f9a5c3f0-5e05-45da-acfc-e03e81a7b36d
# ╟─9cfeb687-dca0-41b7-bcf0-b08d29081c23
# ╟─a225f838-bd6e-425d-b531-d7adf7f3768d
# ╟─52866331-2bdb-4e0d-b8f6-5cfcdff56057
# ╟─54b043c6-7404-448c-8547-afa4288adab2
# ╟─6a10a864-8441-4861-ac31-ced59bf854ac
# ╟─0dafe467-fb06-44e6-91fd-c643df313a9e
# ╟─e748ed86-7bbc-4d30-b6de-283b680f60bb
# ╟─6c9eaa0e-d1b5-4442-993e-d1b6ae15a27d
# ╠═8e4c40f3-26f0-4fb4-b7ae-5c47e7de1cd3
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
